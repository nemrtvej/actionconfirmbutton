<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="/scripts.js"></script>

		<style type="text/css">
			.button {
				border: 1px solid black;
				width: 10em;
				height: 3em;
				display: block;
				text-align: center;
				line-height: 3em;
			}

			.default {
				background-color: green;
			}

			.waiting {
				background-color: grey;
			}

			.confirm {
				background-color: orange;
			}

			.request {
				background-color: cyan;
			}

			.done {
				background-color: blue;
			}

			.error {
				background-color: red;
			}

		</style>
	</head>
	<body>
		<script type="text/javascript">
			$(document).ready(function(){

				var resolution = function(content) {
					var obj = JSON.parse(content);
					if (obj.status == 'OK') {
						return {
							'response': true,
							'message': false,
						}
					} else {
						return {
							'response': false,
							'message': 'Server spadl.',
						}
					}
				}

				$('body').on('click', '.button:not(.initialized)', function(event){

					var actionConfirmButton = new ActionConfirmButton();

					var defaultState = new DefaultState('1) Smazat', 'default');
					var waitingState = new WaitingState('2) ..waiting..', 'waiting', 1000);
					var confirmState = new ConfirmState('3) Opravdu?', 'confirm');
					var requestState = new RequestState('4) -Request-', 'request');
					var doneState    = new    DoneState('5) Done', 'done');
					var errorState   = new   ErrorState('6) Error', 'error');

					defaultState.setNextState(waitingState);
					waitingState.setNextState(confirmState);
					confirmState.setNextState(requestState);
					requestState.setErrorState(errorState);
					requestState.setDoneState(doneState);
					errorState.setNextState(defaultState);
					doneState.setNextState(doneState);


					var htmlInstance = $(this);
					actionConfirmButton.setJqueryNode(htmlInstance);
					actionConfirmButton.setCurrentState(defaultState);

					var options = {
						'method': 'get',
						'url': htmlInstance.attr('href')
					}

					requestState.setRequestOptions(options);
					requestState.setResolutionCallback(resolution);

					$(this).addClass('initialized');

					$(this).on('click', function(event){
						actionConfirmButton.click(event);
					});

					actionConfirmButton.click(event);
				})
			})
		</script>

<?php 
	for ($i = 0; $i<(int)@$_GET['p']; $i++) {
		?>
		<a href="request.php" class="button default">1) Smazat</a>
		<?php
	}
	?>
	<a href="?p=10000">Enter the Matrix!</a>
	</body>
</html>