function ActionConfirmButton() {
	this.currentState;

	this.jqueryNode;

	ActionConfirmButton.prototype.setJqueryNode = function(jqueryNode) {
		this.jqueryNode = jqueryNode;
	}

	ActionConfirmButton.prototype.setCurrentState = function(currentState) {
		if (this.currentState) {
			this.jqueryNode.removeClass(this.currentState.className);
		}
		this.currentState = currentState;
		this.jqueryNode.addClass(this.currentState.className);
		currentState.setActionConfirmButtonInstance(this);

		this.jqueryNode.text(currentState.content);
	}

	ActionConfirmButton.prototype.click = function(event) {
		event.preventDefault();
		this.currentState.click(event);
	}

}


function DefaultState(content, className)
{
	this.content = content;
	this.className = className;

	this.nextState;

	this.actionConfirmButtonInstance;

	DefaultState.prototype.setActionConfirmButtonInstance = function(actionConfirmButtonInstance) {
		this.actionConfirmButtonInstance = actionConfirmButtonInstance;
	}

	DefaultState.prototype.setNextState = function(nextState) {
		this.nextState = nextState;
	}

	DefaultState.prototype.dumpState = function()
	{
		console.log(this.nextState);
	}

	DefaultState.prototype.click = function(event) {
		console.log('default state click');
		this.actionConfirmButtonInstance.setCurrentState(this.nextState);
	}

}

function WaitingState(content, className, duration)
{
	this.content = content;
	this.className = className;
	this.duration = duration;

	this.nextState;

	this.actionConfirmButtonInstance;

	
	WaitingState.prototype.setActionConfirmButtonInstance = function(actionConfirmButtonInstance) {
		this.actionConfirmButtonInstance = actionConfirmButtonInstance;
		var self = this;
		setTimeout(function(){
			self.actionConfirmButtonInstance.setCurrentState(self.nextState);
		}, this.duration);
	}

	WaitingState.prototype.setNextState = function(nextState) {
		this.nextState = nextState;
	}

	WaitingState.prototype.click = function(event) {
		
	}
}

function ConfirmState(content, className)
{
	this.content = content;
	this.className = className;

	this.nextState;

	this.actionConfirmButtonInstance;

	ConfirmState.prototype.setActionConfirmButtonInstance = function(actionConfirmButtonInstance) {
		this.actionConfirmButtonInstance = actionConfirmButtonInstance;
	}

	ConfirmState.prototype.setNextState = function(nextState) {
		this.nextState = nextState;
	}

	ConfirmState.prototype.click = function(event) {
		console.log('confirm state click');
		this.actionConfirmButtonInstance.setCurrentState(this.nextState);
	}
}

function RequestState(content, className, requestObject)
{
	this.content = content;
	this.className = className;


	this.errorState;
	this.doneState;
	this.actionConfirmButtonInstance;
	this.requestOptions = { 'method': 'get', 'url': '' };

	this.resolutionCallback = function(param) {
		 return {
			'response': true,
			'message': false,
		} ;
	}

	RequestState.prototype.setActionConfirmButtonInstance = function(actionConfirmButtonInstance) {
		this.actionConfirmButtonInstance = actionConfirmButtonInstance;
		var xmlHttpRequest = new XMLHttpRequest();
		xmlHttpRequest.open(this.requestOptions.method, this.requestOptions.url);
		xmlHttpRequest.onreadystatechange = stateListener;
		xmlHttpRequest.send();
	}

	RequestState.prototype.setDoneState = function(doneState) {
		this.doneState = doneState;
	}

	RequestState.prototype.setErrorState = function(errorState) {
		this.errorState = errorState;
	}

	RequestState.prototype.setRequestOptions = function(requestOptions) {
		this.requestOptions = requestOptions;
	}

	RequestState.prototype.setResolutionCallback = function(resolutionCallback) {
		this.resolutionCallback = resolutionCallback;
	}

	RequestState.prototype.click = function(event) {		
		// intentionally empty
	}

	var self = this;
	var stateListener = function() {
		if (this.readyState == 4) {
			console.log(this);
			var resolutionResponse = self.resolutionCallback(this.response);
			if (resolutionResponse.response) {
				self.actionConfirmButtonInstance.setCurrentState(self.doneState);
			} else {
				if (resolutionResponse.message) {
					self.errorState.setContent(resolutionResponse.message);
					self.actionConfirmButtonInstance.setCurrentState(self.errorState);
				}
			}
		}
	}
}

function DoneState(content, className)
{
	this.content = content;
	this.className = className;


	this.nextState;

	this.actionConfirmButtonInstance;

	DoneState.prototype.setActionConfirmButtonInstance = function(actionConfirmButtonInstance) {
		this.actionConfirmButtonInstance = actionConfirmButtonInstance;
	}

	DoneState.prototype.setNextState = function(nextState) {
		this.nextState = nextState;
	}

	DoneState.prototype.click = function(event) {
		console.log('done state click');
		this.actionConfirmButtonInstance.setCurrentState(this.nextState);
	}
}

function ErrorState(content, className)
{
	this.content = content;
	this.className = className;


	this.nextState;
	this.actionConfirmButtonInstance;

	ErrorState.prototype.setActionConfirmButtonInstance = function(actionConfirmButtonInstance) {
		this.actionConfirmButtonInstance = actionConfirmButtonInstance;
	}

	ErrorState.prototype.setNextState = function(nextState) {
		this.nextState = nextState;
	}

	ErrorState.prototype.setContent = function(content) {
		this.content = content;
	}

	ErrorState.prototype.click = function(event) {
		console.log('errorState state click');
		this.actionConfirmButtonInstance.setCurrentState(this.nextState);
	}
}